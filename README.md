# TMT Workflow
This workflow handles a complete proteomics data analysis. The input are raw LC-MS files and the output are TMT-quantified protein reports. The main stepas are: 
1.  Create a workspace
2.  Download a database
3.  Search with MSFragger
4.  PeptideProphet
5.  ProteinProphet
6.  Filter
7.  Quantify
8.  Report

More details can be found [here](https://github.com/Nesvilab/philosopher/wiki/Step-by-step-TMT-Analysis).

## Pipeline
The pipeline runs a small tmt workflow and tests for certain output files. Take a look at
`.gitlab-ci.yml` to learn how that exactly works.
