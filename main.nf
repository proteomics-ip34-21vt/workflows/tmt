#!/usr/bin/env nextflow

nextflow.enable.dsl=2

params.input_dir = ''
params.output_dir = ''
params.ram = ''
params.db_id = '' // human: UP000005640
params.custom_fasta = ''
params.annotation = '10'

include { INIT_WORKSPACE } from './modules/init_workspace'
include { MSFRAGGER } from './modules/msfragger'
include { BUILD_FASTA } from './modules/build_fasta'
include { PEPTIDE_PROPHET } from './modules/peptide_prophet'
include { PROTEIN_PROPHET } from './modules/protein_prophet'
include { FILTER_FDR } from './modules/filter_fdr'
include { LABELQUANT } from './modules/labelquant'
include { REPORT } from './modules/report'
include { FLAG_CUSTOMS } from './modules/flag_customs.nf'

workflow {
	def params_cmd = '''
	sed -i -e 's/# variable_mod_03.*/variable_mod_03 = 79.966331 STY 3/' closed_fragger.params
	sed -i -e 's/# variable_mod_04.*/variable_mod_04 = 229.162932 n^ 1/' closed_fragger.params
	echo "add_K_lysine_ = 229.162932" >> closed_fragger.params
	'''

	INIT_WORKSPACE()

	custom_fasta = params.custom_fasta
	if (!custom_fasta) {
		file = File.createTempFile("nf_temp", ".fasta")
		custom_fasta = file.absolutePath
	}

	BUILD_FASTA(INIT_WORKSPACE.out.meta, params.db_id, custom_fasta)
	MSFRAGGER(BUILD_FASTA.out.db, params.input_dir, params.ram, "closed", params_cmd)
	PEPTIDE_PROPHET(BUILD_FASTA.out.meta, BUILD_FASTA.out.db, MSFRAGGER.out.pepXML, "--ppm --accmass --expectscore --decoyprobs --decoy rev_ --nonparam")
	PROTEIN_PROPHET(PEPTIDE_PROPHET.out.meta, BUILD_FASTA.out.db, PEPTIDE_PROPHET.out.pep_xml, "")

	group_dirs = Channel.fromPath(params.input_dir + "/*", type:"dir" ) // from here workflows will run parallel with one item of 'group_dirs'
	FILTER_FDR(PROTEIN_PROPHET.out.meta, PEPTIDE_PROPHET.out.pep_xml, PROTEIN_PROPHET.out.interact_prot, "--razor", group_dirs)
	LABELQUANT(FILTER_FDR.out, PEPTIDE_PROPHET.out.pep_xml, PROTEIN_PROPHET.out.interact_prot, params.annotation)
	REPORT(LABELQUANT.out, PEPTIDE_PROPHET.out.pep_xml, PROTEIN_PROPHET.out.interact_prot)

	custom_fasta = params.custom_fasta
	if (custom_fasta) {
		FLAG_CUSTOMS(custom_fasta, REPORT.out.report_dir)
	}
}
